package com.qa.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;

//Used for WD init and props
public class TestBase {
	public static WebDriver driver;
	public static Properties properties;
	FileInputStream fileInputStream;
	static String userDirectoryPath;

	public TestBase() {
		try {
			userDirectoryPath = System.getProperty("user.dir");
			properties = new Properties();
			String filePath = userDirectoryPath
					+ "/src/main/java/com/qa/config/config.properties";
			fileInputStream = new FileInputStream(filePath);
			properties.load(fileInputStream);
			fileInputStream.close();
		} catch (IOException e) {
			e.getMessage();
		}
	}

	public static void initialization() {

		String browserName = properties.getProperty("browser");
		// Use Factory design pattern to initialize and return browser instance
		if (browserName.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver", userDirectoryPath
					+ "/src/test/resources/drivers/chromedriver.exe");
			driver = new ChromeDriver();
		}
		if (browserName.equals("edge")) {
			System.setProperty("webdriver.chrome.driver", userDirectoryPath
					+ "/src/test/resources/drivers/msedgedriver.exe");
			driver = new EdgeDriver();
		}

		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts()
				.implicitlyWait(Duration.ofSeconds(TestUtil.IMPLICIT_SECONDS));
		driver.manage()
				.timeouts()
				.pageLoadTimeout(Duration.ofSeconds(TestUtil.PAGE_LOAD_TIMEOUT));
		driver.get(properties.getProperty("URL"));
	}

	public static void tearDown() {
		driver.close();
		driver.quit();
	}
}
