package com.qa.pages;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.util.TestBase;

public class SignInPage extends TestBase {
	private By emailTextBox = By.id("username");
	private By passwordTextBox = By.id("password");
	private By signInButton = By.id("tp-sign-in");

	public SignInPage() {
		PageFactory.initElements(driver, this);
	}

	public String validateSignInPageTitle() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
		wait.until(ExpectedConditions.presenceOfElementLocated(signInButton));
		return driver.getTitle();
	}

	public MyTestsPage userLogsIn(String username, String password) {
		userEntersUsername(username);
		userEntersPassword(password);
		driver.findElement(signInButton).click();
		return new MyTestsPage();
	}

	public void userEntersUsername(String username) {
		driver.findElement(emailTextBox).sendKeys(username);
	}

	public void userEntersPassword(String password) {
		driver.findElement(passwordTextBox).sendKeys(password);
	}

}
