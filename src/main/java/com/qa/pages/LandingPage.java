package com.qa.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.TestBase;

public class LandingPage extends TestBase {
	private By loginLink = By.xpath("//a[contains(text(),'Login')]");

	public LandingPage() {
		PageFactory.initElements(driver, this);
	}

	public String validateLandingPageTitle() {
		return driver.getTitle();
	}

	public SignInPage userClicksLoginLink() {
		driver.findElement(loginLink).click();

		handleWindows();

		return new SignInPage();
	}

	private void handleWindows() {
		List<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(0));
		driver.close();
		driver.switchTo().window(tabs2.get(1));
	}
}
