package com.qa.pages;

import org.openqa.selenium.support.PageFactory;

import com.qa.util.TestBase;

public class MyTestsPage extends TestBase  {

	public MyTestsPage() {
		PageFactory.initElements(driver, this);
	}

	public String validateTestsPageTitle() {
		return driver.getTitle();
	}
}
