#Author: Shibi Kuriakose
#Keywords Summary: This is used to login to TestProject site.

Feature: To login to TestProject site.

  Scenario: Validate user can log in to testproject site
    Given user is on testproject landing page
    When user clicks on login link
    Then user is logged in to testproject account