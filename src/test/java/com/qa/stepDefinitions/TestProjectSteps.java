package com.qa.stepDefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.qa.pages.LandingPage;
import com.qa.pages.MyTestsPage;
import com.qa.pages.SignInPage;
import com.qa.util.TestBase;

//Assertions, business logic verification done here.
public class TestProjectSteps extends TestBase {
	LandingPage landingpage;
	SignInPage signInPage;
	MyTestsPage myTestsPage;

	@Given("^user is on testproject landing page$")
	public void user_is_on_testproject_landing_page() {
		TestBase.initialization();
		
		landingpage = new LandingPage();
		
		String landingPageTitle = landingpage.validateLandingPageTitle();
		Assert.assertEquals("Free Test Automation For All | TestProject",
				landingPageTitle);
	}

	@When("^user clicks on login link$")
	public void user_clicks_on_login_link() {
		signInPage = landingpage.userClicksLoginLink();
	
		String signInPageTitle = signInPage.validateSignInPageTitle();
		Assert.assertEquals("TestProject - Login", signInPageTitle);
	}

	@Then("^user is logged in to testproject account$")
	public void user_is_logged_in_to_testproject_account() {
		String username = TestBase.properties.getProperty("username");
		String password = TestBase.properties.getProperty("password");
		
		myTestsPage = signInPage.userLogsIn(username, password);
		
		String testsPageTitle = myTestsPage.validateTestsPageTitle();
		Assert.assertEquals("TestProject", testsPageTitle);
		
		WebElement myFirstTestLink = driver.findElement(By.cssSelector(".item-name-test-name"));
		Assert.assertEquals("My First Test", myFirstTestLink.getText());
		
		TestBase.tearDown();
	}

}
