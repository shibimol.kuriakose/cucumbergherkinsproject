package com.qa.runner;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/com/qa/features", 
glue = { "com/qa/stepDefinitions" },
monochrome = true,
dryRun = false,
plugin = {"pretty", "json:target/JSONReports/report.json"})
public class TestRunner {

}
